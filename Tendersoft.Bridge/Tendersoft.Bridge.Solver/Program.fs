﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.
//printfn "The winner is: %A" (CardGameWar.simulation 4)
//    printfn "%A" generateDeck
//    printfn "%A" (generateDeck |> List.map CardString )
 // 3NT
  // return an integer exit code
module Program

open Other
open CardGameWar
open Tendersoft.Bridge.Engine.Core

[<EntryPoint>]
let main argv = 
  System.Console.OutputEncoding <- System.Text.Encoding.Unicode
  printfn "%A" argv
  let deck = generateSuffledDeck()
//  printfn "%A" deck
  printfn "%A" (deck |> deckStrings)
//  for (_,s) in deck do
//    printf "(%O %s)" s (s.ToString())
  Tendersoft.Bridge.Engine.Drawing.printDeck deck
  let playerN = 
    { Name = "N"
      Side = Side.N
      Hand = 
        [ (Rank.RA, Suit.S)
          (Rank.RK, Suit.S) ]
      Tricks = [] }
  
  let playerW = 
    { Name = "W"
      Side = Side.W
      Hand = 
        [ (Rank.R2, Suit.D)
          (Rank.RQ, Suit.C) ]
      Tricks = [] }
  
  let playerS = 
    { Name = "S"
      Side = Side.S
      Hand = 
        [ (Rank.RA, Suit.C)
          (Rank.RK, Suit.C) ]
      Tricks = [] }
  
  let playerE = 
    { Name = "E"
      Side = Side.E
      Hand = 
        [ (Rank.RQ, Suit.S)
          (Rank.RJ, Suit.S) ]
      Tricks = [] }
  
  let contract = 3, Option<Suit>.None
  
  let table = 
    { Players = [| playerN; playerW; playerS; playerE |]
      Contract = contract
      Tricks = []
      Dealer = playerS
      Vulnerable = None
      Auction=[] }
  //printfn "%A" table
  //let tableS = sortTablePlayers table
  //printfn "%A" tableS
  let solutions = solve2 table
  printfn "%A" solutions
  0
