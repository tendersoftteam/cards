﻿namespace Tendersoft.Bridge.Engine.Tests

module CoreTests = 
  open Xunit
  //open FsUnit.Xunit
  //open FsCheck
  open Tendersoft.Bridge.Engine.Core
  
  // open 
  [<Fact>]
  let ``card deck has 52 cards``() = 
    generateDeck
    |> List.length
    |> fun x -> Assert.Equal(52, x)
  
  [<Fact>]
  let ``shuffledDeck is different than non-shuffled one``() = 
    let genDeck = generateDeck //|> List.toSeq
    let shuffDeck = generateSuffledDeck() //|> List.toSeq
    Assert.NotEqual<Card>(genDeck, shuffDeck)
  
  [<Fact>]
  let ``shuffledDeck is unique``() = 
    generateSuffledDeck()
    |> isUnique
    |> Assert.True
  
  // suit natural order comparisson
  [<Fact>]
  let ``Clubs are equal (suits)``() = 
    let comparissonResult = suitsComparerNatural Suit.C Suit.C
    Assert.Equal(0, comparissonResult)
  
  [<Fact>]
  let ``Spades are higher than Clubs (suits)``() = 
    let comparissonResult = suitsComparerNatural Suit.D Suit.C
    Assert.Equal(1, comparissonResult)
  
  [<Fact>]
  let ``Clubs are lower than Diamonds (suits)``() = 
    let comparissonResult = suitsComparerNatural Suit.C Suit.D
    Assert.Equal(-1, comparissonResult)
  
  //suit general comparer  
  [<Fact>]
  let ``Clubs are higher than Diamonds (vist:Clubs)``() = 
    let suitOrdering suit = [ Suit.C; Suit.D ] |> List.findIndex (fun s -> s = suit)
    let comparissonResult = suitsComparerGeneral suitOrdering Suit.C Suit.D
    Assert.Equal(1, comparissonResult)
  
  [<Fact>]
  let ``Spades are lower than Diamonds (atu:Diamonds)``() = 
    let suitOrdering suit = [ Suit.C; Suit.D; Suit.S ] |> List.findIndex (fun s -> s = suit)
    let comparissonResult = suitsComparerGeneral suitOrdering Suit.S Suit.D
    Assert.Equal(-1, comparissonResult)
  
  //rank comparisson tests
  [<Fact>]
  let ``Aces are equal (ranks)``() = 
    let comparissonResult = ranksComparer Rank.RA Rank.RA
    Assert.Equal(0, comparissonResult)
  
  [<Fact>]
  let ``Ace is higher than king (ranks)``() = 
    let comparissonResult = ranksComparer Rank.RA Rank.RK
    Assert.Equal(1, comparissonResult)
  
  [<Fact>]
  let ``2 is lower than 3 (ranks)``() = 
    let comparissonResult = ranksComparer Rank.R2 Rank.R3
    Assert.Equal(-1, comparissonResult)
  
  // card natural order comparisson tests
  [<Fact>]
  let ``Club aces are equal (cards)``() = 
    let comparissonResult = cardsComparerNatural (Rank.RA, Suit.C) (Rank.RA, Suit.C)
    Assert.Equal(0, comparissonResult)
  
  [<Fact>]
  let ``Club ace is lower than Diamond 2 (cards)``() = 
    let comparissonResult = cardsComparerNatural (Rank.RA, Suit.C) (Rank.R2, Suit.D)
    Assert.Equal(-1, comparissonResult)
  
  [<Fact>]
  let ``Club King is lower than Club Ace``() = 
    let comparissonResult = cardsComparerNatural (Rank.RK, Suit.C) (Rank.RA, Suit.C)
    Assert.Equal(-1, comparissonResult)
  
  [<Fact>]
  let ``Club King is lower than Spade Ace``() = 
    let comparissonResult = cardsComparerNatural (Rank.RK, Suit.C) (Rank.RA, Suit.S)
    Assert.Equal(-1, comparissonResult)
  
  // card comparisson with Atu test 
  [<Fact>]
  let ``Clubs are higher than Spades (Atu:Clubs, vist:Diamonds)``() = 
    let comparissonResult = suitsComparerWithAtu (Some Suit.C) (None, Suit.D) Suit.C Suit.S
    Assert.Equal(1, comparissonResult)
  
  [<Fact>]
  let ``Club aces are equal (atu:Spades, vist:Hearts)``() = 
    let comparissonResult = cardsComparerWithAtu (Some Suit.S) (None, Suit.H) (Rank.RA, Suit.C) (Rank.RA, Suit.C)
    Assert.Equal(0, comparissonResult)
  
  [<Fact>]
  let ``Club ace is lower than Diamond 2 (atu:Spades, vist:Hearts)``() = 
    let comparissonResult = cardsComparerWithAtu (Some Suit.S) (None, Suit.H) (Rank.RA, Suit.C) (Rank.R2, Suit.D)
    Assert.Equal(-1, comparissonResult)
  
  [<Fact>]
  let ``Club King is lower than Club Ace (Atu:Spades, vist:Diamonds)``() = 
    let comparissonResult = cardsComparerWithAtu (Some Suit.S) (None, Suit.D) (Rank.RK, Suit.C) (Rank.RA, Suit.C)
    Assert.Equal(-1, comparissonResult)
  
  [<Fact>]
  let ``Club 2 is higher than Spade Ace (Atu:Clubs, vist:Diamonds)``() = 
    let comparissonResult = cardsComparerWithAtu (Some Suit.C) (None, Suit.D) (Rank.R2, Suit.C) (Rank.RA, Suit.S)
    Assert.Equal(1, comparissonResult)
  
  //card ordering tests
  [<Fact>]
  let ``Trick (1) natural ordering is OK``() = 
    let currentTrick = [ (Rank.RA, Suit.C) ]
    printf "%O" currentTrick
    let result = sortCards currentTrick
    let expected = currentTrick
    Assert.Equal<Card list>(expected, result)
  
  [<Fact>]
  let ``Trick (2a) natural ordering is OK``() = 
    let currentTrick = 
      [ (Rank.R2, Suit.D)
        (Rank.RA, Suit.C) ]
    printf "%O" currentTrick
    let result = sortCards currentTrick
    let expected = currentTrick
    Assert.Equal<Card list>(expected, result)
  
  [<Fact>]
  let ``Trick (2b) natural ordering is OK``() = 
    let currentTrick = 
      [ (Rank.RA, Suit.C)
        (Rank.R2, Suit.D) ]
    printf "%O" currentTrick
    let result = sortCards currentTrick
    
    let expected = 
      [ (Rank.R2, Suit.D)
        (Rank.RA, Suit.C) ]
    Assert.Equal<Card list>(expected, result)
  
  [<Fact>]
  let ``Trick (3a) natural ordering is OK``() = 
    let currentTrick = 
      [ (Rank.RA, Suit.C)
        (Rank.R2, Suit.D)
        (Rank.R3, Suit.S) ]
    printf "%O" currentTrick
    let result = sortCards currentTrick
    
    let expected = 
      [ (Rank.R3, Suit.S)
        (Rank.R2, Suit.D)
        (Rank.RA, Suit.C) ]
    Assert.Equal<Card list>(expected, result)
  
  [<Fact>]
  let ``Trick (3b) natural ordering is OK``() = 
    let currentTrick = 
      [ (Rank.R3, Suit.S)
        (Rank.R2, Suit.D)
        (Rank.RA, Suit.C) ]
    printf "%O" currentTrick
    let result = sortCards currentTrick
    let expected = currentTrick
    Assert.Equal<Card list>(expected, result)
  
  //trick ordering tests
  [<Fact>]
  let ``Trick (1) (suit:NT) ordering is OK``() = 
    let currentTrick = [ (Rank.RA, Suit.C) ]
    printf "%O" currentTrick
    let result = sortTrickCards (None) currentTrick
    let expected = currentTrick
    Assert.Equal<Card list>(expected, result)
  
  [<Fact>]
  let ``Trick (2a) (suit:NT) ordering is OK``() = 
    let currentTrick = 
      [ (Rank.R2, Suit.D)
        (Rank.RA, Suit.C) ]
    printf "%O" currentTrick
    let result = sortTrickCards (None) currentTrick
    let expected = currentTrick
    Assert.Equal<Card list>(expected, result)
  
  [<Fact>]
  let ``Trick (2b) (suit:NT) ordering is OK``() = 
    let currentTrick = 
      [ (Rank.RA, Suit.C)
        (Rank.R2, Suit.D) ]
    printf "%O" currentTrick
    let result = sortTrickCards (None) currentTrick
    let expected = currentTrick
    Assert.Equal<Card list>(expected, result)
  
  [<Fact>]
  let ``Trick (3a) (atu:NT) ordering is OK``() = 
    let currentTrick = 
      [ (Rank.RA, Suit.C)
        (Rank.R2, Suit.D)
        (Rank.R3, Suit.S) ]
    printf "%O" currentTrick
    let result = sortTrickCards (None) currentTrick
    
    let expected = 
      [ (Rank.RA, Suit.C)
        (Rank.R3, Suit.S)
        (Rank.R2, Suit.D) ] //expected = currentTrick <=> optimized to not sort irrelevant suits
    Assert.Equal<Card list>(expected, result)
  
  [<Fact>]
  let ``Trick (3b) (atu:Diamonds) ordering is OK``() = 
    let currentTrick = 
      [ (Rank.RA, Suit.C)
        (Rank.R2, Suit.D)
        (Rank.R3, Suit.S) ]
    printf "%O" currentTrick
    let result = sortTrickCards (Some Suit.D) currentTrick
    
    let expected = 
      [ (Rank.R2, Suit.D)
        (Rank.RA, Suit.C)
        (Rank.R3, Suit.S) ]
    Assert.Equal<Card list>(expected, result)
  
  //sort table hands tests
  [<Fact>]
  let ``hand sorting is side effects free``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let table = createTable players (3, Some Suit.D)
    
    let p0hand = 
      [ (Rank.RK, Suit.C)
        (Rank.R2, Suit.D)
        (Rank.R3, Suit.D)
        (Rank.RA, Suit.C)
        (Rank.R2, Suit.S)
        (Rank.RA, Suit.H) ]
    table.Players.[0] <- { table.Players.[0] with Hand = p0hand }
    let table0 = sortTableHands table
    
    let expected = 
      [ (Rank.R2, Suit.S)
        (Rank.RA, Suit.H)
        (Rank.R3, Suit.D)
        (Rank.R2, Suit.D)
        (Rank.RA, Suit.C)
        (Rank.RK, Suit.C) ]
    Assert.NotEqual(table, table0)
    Assert.NotEqual<Card list>(expected, table.Players.[0].Hand)
  
  [<Fact>]
  let ``Player0 hand is ordered``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let table = createTable players (3, Some Suit.D)
    
    let p0hand = 
      [ (Rank.RK, Suit.C)
        (Rank.R2, Suit.D)
        (Rank.R3, Suit.D)
        (Rank.RA, Suit.C)
        (Rank.R2, Suit.S)
        (Rank.RA, Suit.H) ]
    table.Players.[0] <- { table.Players.[0] with Hand = p0hand }
    let table = sortTableHands table
    
    let expected = 
      [ (Rank.R2, Suit.S)
        (Rank.RA, Suit.H)
        (Rank.R3, Suit.D)
        (Rank.R2, Suit.D)
        (Rank.RA, Suit.C)
        (Rank.RK, Suit.C) ]
    Assert.Equal<Card list>(expected, table.Players.[0].Hand)
  
  //trick winner tests
  [<Fact>]
  let ``Player2 wins with Atu``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let table = createTable players (3, Some Suit.D)
    
    let currentTrick = 
      [ (Rank.RA, Suit.C)
        (Rank.R2, Suit.D) ]
    
    let _, atu = table.Contract
    let wpi = winningPlayerIndex table.Players atu 1 currentTrick
    Assert.Equal(2, wpi)
  
  [<Fact>]
  let ``Player1 wins with greater Atu``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let table = createTable players (3, Some Suit.D)
    
    let currentTrick = 
      [ (Rank.RK, Suit.C)
        (Rank.R2, Suit.D)
        (Rank.R3, Suit.D)
        (Rank.RA, Suit.C) ]
    
    let _, atu = table.Contract
    let wpi = winningPlayerIndex table.Players atu 3 currentTrick
    Assert.Equal(1, wpi)
  
  [<Fact>]
  let ``Player0 wins with best color``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let table = createTable players (3, Some Suit.D)
    
    let currentTrick = 
      [ (Rank.RQ, Suit.C)
        (Rank.RK, Suit.S)
        (Rank.RK, Suit.C)
        (Rank.RA, Suit.H) ]
    
    let _, atu = table.Contract
    let wpi = winningPlayerIndex table.Players atu 2 currentTrick
    Assert.Equal(0, wpi)
  
  [<Fact>]
  let ``table with 1 trick gets played OK``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let table = createTable players (3, Some Suit.D)
    
    let currentTrick = 
      [ (Rank.RQ, Suit.C)
        (Rank.RK, Suit.S)
        (Rank.RK, Suit.C)
        (Rank.RA, Suit.H) ]
    for i = 0 to table.Players.Length - 1 do
      table.Players.[i] <- { table.Players.[i] with Hand = [ currentTrick.[i] ] }
    let playedTable = playTrickRound table 2
    Assert.NotEqual(table, playedTable)
    Assert.All(playedTable.Players, fun p -> Assert.Empty p.Hand)
    Assert.Single(playedTable.Tricks, 
                  [ (Rank.RK, Suit.C)
                    (Rank.RA, Suit.H)
                    (Rank.RQ, Suit.C)
                    (Rank.RK, Suit.S) ])
    Assert.Equal(4, playedTable.Tricks.[0].Length)
  
  [<Fact>]
  let ``table with 13 tricks get played OK``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let table = createTable players (3, Some Suit.D)
    let deck = generateDeck // generateSuffledDeck()
    let table = dealDeck2 deck table
    let playedTable = playTrickRound table 0
    Assert.NotEqual(table, playedTable)
    Assert.All(playedTable.Players, fun p -> Assert.Empty p.Hand)
    Assert.Equal(13, playedTable.Tricks.Length)
    Assert.All(playedTable.Tricks, fun t -> Assert.Equal(4, t.Length))
  
  //playerTrickWinCount tests  
  [<Fact>]
  let ``Player 2 wins 4 of 4 tricks``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let cards = generateDeck |> List.take 16
    let table = createTable players (3, Some Suit.D) |> dealDeck2 cards
    
    //      [ (Rank.RA, Suit.D)
    //        (Rank.RA, Suit.S)
    //        (Rank.RA, Suit.H)
    //        (Rank.RA, Suit.C)
    //        (Rank.RK, Suit.D)
    //        (Rank.RK, Suit.S)
    //        (Rank.RK, Suit.H)
    //        (Rank.RK, Suit.C)
    //        (Rank.RQ, Suit.D)
    //        (Rank.RQ, Suit.S)
    //        (Rank.RQ, Suit.H)
    //        (Rank.RQ, Suit.C)
    //        (Rank.RJ, Suit.D)
    //        (Rank.RJ, Suit.S)
    //        (Rank.RJ, Suit.H)
    //        (Rank.RJ, Suit.C) ]
    let playedTables = 
      solve table
      |> Seq.take 3
      |> Seq.cache
    
    let wc = 
      playedTables
      |> Seq.item 2
      |> fun t -> playerTrickWinCount t 2
    
    Assert.Equal(4, wc)
    Assert.Equal(3, playedTables |> Seq.length)
    Assert.All(playedTables, fun t -> Assert.NotEqual(table, t))
    Assert.All(playedTables, fun t -> Assert.Equal(4, t.Tricks.Length))
    Assert.All(playedTables, fun t -> Assert.All(t.Players, fun p -> Assert.Empty p.Hand))
    Assert.All(playedTables, fun t -> Assert.All(t.Tricks, fun tr -> Assert.Equal(4, tr.Length)))
  
  //generatePlayedTrickTables tests  
  [<Fact>]
  let ``table with 1 trick produces 1 possible table after 1 full trick``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let table = createTable players (3, Some Suit.D)
    
    let currentTrick = 
      [ (Rank.RQ, Suit.C)
        (Rank.RK, Suit.S)
        (Rank.RK, Suit.C)
        (Rank.RA, Suit.H) ]
    for i = 0 to table.Players.Length - 1 do
      table.Players.[i] <- { table.Players.[i] with Hand = [ currentTrick.[i] ] }
    let playedTables = generatePlayedTrickTables table |> Seq.cache
    Assert.Equal(1, playedTables |> Seq.length)
    let playedTable = playedTables |> Seq.head
    Assert.NotEqual(table, playedTable)
    Assert.All(playedTable.Players, fun p -> Assert.Empty p.Hand)
    Assert.Single(playedTable.Tricks, currentTrick)
    Assert.Equal(4, playedTable.Tricks.[0].Length)
  
  //possibleTables tests  
  [<Fact>]
  let ``table with 1 trick produces 1 possible table``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let table = createTable players (3, Some Suit.D)
    
    let currentTrick = 
      [ (Rank.RQ, Suit.C)
        (Rank.RK, Suit.S)
        (Rank.RK, Suit.C)
        (Rank.RA, Suit.H) ]
    for i = 0 to table.Players.Length - 1 do
      table.Players.[i] <- { table.Players.[i] with Hand = [ currentTrick.[i] ] }
#if DEBUG2
    let playedTables = possibleTables table |> Seq.cache
#else
    let playedTables = possibleTables table |> Seq.cache
#endif
    
    Assert.Equal(1, playedTables |> Seq.length)
    let playedTable = playedTables |> Seq.head
    Assert.NotEqual(table, playedTable)
    Assert.All(playedTable.Players, fun p -> Assert.Empty p.Hand)
    Assert.Single(playedTable.Tricks, currentTrick)
    Assert.Equal(4, playedTable.Tricks.[0].Length)
  
  //solve tests  
  [<Fact>]
  let ``table with 1 trick gets solved OK``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let table = createTable players (3, Some Suit.D)
    
    let currentTrick = 
      [ (Rank.RQ, Suit.C)
        (Rank.RK, Suit.S)
        (Rank.RK, Suit.C)
        (Rank.RA, Suit.H) ]
    for i = 0 to table.Players.Length - 1 do
      table.Players.[i] <- { table.Players.[i] with Hand = [ currentTrick.[i] ] }
    let playedTables = solve table |> Seq.cache
    Assert.Equal(1, playedTables |> Seq.length)
    let playedTable = playedTables |> Seq.head
    Assert.NotEqual(table, playedTable)
    Assert.All(playedTable.Players, fun p -> Assert.Empty p.Hand)
    Assert.Single(playedTable.Tricks, currentTrick)
    Assert.Equal(4, playedTable.Tricks.[0].Length)
  
  [<Fact>]
  let ``table with 2 tricks gets solved OK``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let table = createTable players (3, Some Suit.D)
    
    let currentTrick = 
      [ (Rank.RQ, Suit.C)
        (Rank.RK, Suit.S)
        (Rank.RK, Suit.C)
        (Rank.RA, Suit.H)
        (Rank.R2, Suit.D)
        (Rank.R3, Suit.S)
        (Rank.R4, Suit.C)
        (Rank.R5, Suit.D) ]
    for i = 0 to table.Players.Length - 1 do
      table.Players.[i] <- { table.Players.[i] with Hand = 
                                                      [ currentTrick.[2 * i]
                                                        currentTrick.[2 * i + 1] ] }
    let playedTables = solve table |> Seq.cache
    Assert.Equal(6, playedTables |> Seq.length)
    Assert.All(playedTables, fun t -> Assert.NotEqual(table, t))
    Assert.All(playedTables, fun t -> Assert.Equal(2, t.Tricks.Length))
    Assert.All(playedTables, fun t -> Assert.All(t.Players, fun p -> Assert.Empty p.Hand))
    Assert.All(playedTables, fun t -> Assert.All(t.Tricks, fun tr -> Assert.Equal(4, tr.Length)))
  
  open FSharp.Collections.ParallelSeq
  open System.Collections.Generic
  open System.Collections.Concurrent
  open System.Linq
  open Newtonsoft.Json
  open System.Security.Cryptography
  
  [<Fact>]
  let ``table with 13 tricks gets solved OK``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let cardLimit = 4 //24
    
    let deck = 
      generateDeck
      |> List.take cardLimit
      |> shuffle
    Tendersoft.Bridge.Engine.Drawing.printDeck deck
    let table = createTable players (3, Some Suit.D) |> dealDeck2 deck
    let playedTables = solve table //|> ParallelEnumerable.AsParallel //|> Seq.cache
    
    //Assert.Equal(6, playedTables |> Seq.length)
    //    for t in playedTables do
    //      Assert.NotEqual(table, t)
    //      Assert.Equal(13, t.Tricks.Length)
    //      Assert.All(t.Players, fun p -> Assert.Empty p.Hand)
    //      Assert.All(t.Tricks, fun tr -> Assert.Equal(4, tr.Length))
    let ps = 
      playedTables
      |> Seq.chunkBySize 100
      |> ParallelEnumerable.AsParallel
    
    let ps = playedTables.AsParallel()
    
    let assertFunc t = 
      Assert.NotEqual(table, t)
      Assert.Equal(cardLimit / 4, t.Tricks.Length)
      Assert.All(t.Players, fun p -> Assert.Empty p.Hand)
      Assert.All(t.Tricks, fun tr -> Assert.Equal(4, tr.Length))
    
    let results = new ConcurrentDictionary<int, int>()
    let tablesHashset = new HashSet<int>()
    let duplicateCount = ref 0
    let tableCount = ref 0
    
    let countResult t = 
      let td = (playResult t).TricksDelta
      System.Threading.Interlocked.Increment tableCount |> ignore
      results.AddOrUpdate(td, 1, fun _ v -> v + 1)
    
    let sha256 (t : Table) = 
      JsonConvert.SerializeObject t
      |> System.Text.Encoding.UTF8.GetBytes
      |> (new SHA256Managed()).ComputeHash
    
    //|> Seq.iter (printf "%x")
    //let analyseTables (t:Table) =
    //let thash = t.GetHashCode()//sha256 t// 
    //let hashAdded = lock tablesHashset (fun _ -> tablesHashset.Add t)
    //      if not hashAdded then 
    //        System.Threading.Interlocked.Increment duplicateCount |> ignore
    //let t1 = tablesHashset.SingleOrDefault(fun ths -> t.Equals(ths))
    //printf "%A" t1
    //else printf "T%A/%d+%d " thash tablesHashset.Count !duplicateCount
    ParallelEnumerable.ForAll
      (ps, 
       fun t -> 
         //for t in playedTables do
         //assertFunc t//)
         countResult t |> ignore
         if (!tableCount) % 1000 = 0 then 
           printf "tables: %A " (results.OrderByDescending(fun kv -> kv.Key) |> sprintf "%A;"))
    //analyseTables t)
    printf "all tables: %A " (results.Values.Sum(), results.Keys.Count, results.Keys.Max())
  
  //[<Fact>]
  let ``table with 3of4 tricks gets solved OK``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let giveHandsToPlayers = giveHands players
    giveHandsToPlayers [ Side.N, 
                         [ (Rank.RT, Suit.S)
                           (Rank.R8, Suit.S)
                           (Rank.R5, Suit.S)
                           (Rank.R6, Suit.H) ] //(Rank.R3,Suit.H);]//(Rank.R7,Suit.D);]
                         Side.E, 
                         [ (Rank.R4, Suit.S)
                           (Rank.R2, Suit.S)
                           (Rank.RK, Suit.H)
                           (Rank.R2, Suit.H) ] //(Rank.R8,Suit.D);]//(Rank.R3,Suit.C);]
                         Side.S, 
                         [ (Rank.RA, Suit.S)
                           (Rank.R7, Suit.S)
                           (Rank.R3, Suit.S)
                           (Rank.RA, Suit.H) ] //(Rank.RQ,Suit.H);]//(Rank.R4,Suit.D);]
                         Side.W, 
                         [ (Rank.RK, Suit.S)
                           (Rank.RQ, Suit.S)
                           (Rank.RK, Suit.C)
                           (Rank.RQ, Suit.C) ] //(Rank.R6,Suit.C);]//(Rank.R2,Suit.C);]
                                               ]
    let table = createTable players (1, None)
    let playedTables = solve3 table
    let ps = playedTables.AsParallel()
    
    let assertFunc t = 
      Assert.NotEqual(table, t)
      Assert.Equal(4, t.Tricks.Length)
      Assert.All(t.Players, fun p -> Assert.Empty p.Hand)
      Assert.All(t.Tricks, fun tr -> Assert.Equal(4, tr.Length))
    
    let results = new ConcurrentDictionary<int, int>()
    let tableCount = ref 0
    
    let countResult t = 
      let td = (playResult t).TricksDelta
      System.Threading.Interlocked.Increment tableCount |> ignore
      results.AddOrUpdate(td, 1, fun _ v -> v + 1)
    //    ParallelEnumerable.ForAll(ps, 
    //                              fun t -> 
    for t in playedTables do
      assertFunc t //)
      countResult t |> ignore
      if (!tableCount) % 1000 = 0 then 
        printf "tables: %A " (results.OrderByDescending(fun kv -> kv.Key) |> sprintf "%A;") //)
    //analyseTables t)
    printf "all tables: %A " (results.Values.Sum(), results.Keys.Count, results.Keys.Max())
  
  //[<Fact>]
  let ``table with 5of6 tricks gets solved OK``() = 
    let players = Side.EnumerateAll |> Array.map createPlayer
    let giveHandsToPlayers = giveHands players
    giveHandsToPlayers [ Side.N, 
                         [ (Rank.RT, Suit.S)
                           (Rank.R8, Suit.S)
                           (Rank.R5, Suit.S)
                           (Rank.R6, Suit.H)
                           (Rank.R3, Suit.H)
                           (Rank.R7, Suit.D) ]
                         Side.E, 
                         [ (Rank.R4, Suit.S)
                           (Rank.R2, Suit.S)
                           (Rank.RK, Suit.H)
                           (Rank.R2, Suit.H)
                           (Rank.R8, Suit.D)
                           (Rank.R3, Suit.C) ]
                         Side.S, 
                         [ (Rank.RA, Suit.S)
                           (Rank.R7, Suit.S)
                           (Rank.R3, Suit.S)
                           (Rank.RA, Suit.H)
                           (Rank.RQ, Suit.H)
                           (Rank.R4, Suit.D) ]
                         Side.W, 
                         [ (Rank.RK, Suit.S)
                           (Rank.RQ, Suit.S)
                           (Rank.RK, Suit.C)
                           (Rank.RQ, Suit.C)
                           (Rank.R6, Suit.C)
                           (Rank.R2, Suit.C) ] ]
    let table = createTable players (1, None)
    let playedTables = solve3 table
    let ps = playedTables.AsParallel()
    
    let assertFunc t = 
      Assert.NotEqual(table, t)
      Assert.Equal(6, t.Tricks.Length)
      Assert.All(t.Players, fun p -> Assert.Empty p.Hand)
      Assert.All(t.Tricks, fun tr -> Assert.Equal(4, tr.Length))
    
    let results = new ConcurrentDictionary<int, int>()
    let tableCount = ref 0
    
    let countResult t = 
      let td = (playResult t).TricksDelta
      System.Threading.Interlocked.Increment tableCount |> ignore
      results.AddOrUpdate(td, 1, fun _ v -> v + 1)
    //    ParallelEnumerable.ForAll(ps, 
    //                              fun t -> 
    for t in playedTables do
      assertFunc t //)
      countResult t |> ignore
      if (!tableCount) % 1000 = 0 then 
        printf "tables: %A " (results.OrderByDescending(fun kv -> kv.Key) |> sprintf "%A;") //)
    //analyseTables t)
    printf "all tables: %A " (results.Values.Sum(), results.Keys.Count, results.Keys.Max())
