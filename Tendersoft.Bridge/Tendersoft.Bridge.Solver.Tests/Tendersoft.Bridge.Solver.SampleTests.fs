﻿namespace Tendersoft.Bridge.Solver.Tests

module SampleTests = 
  // https://github.com/fsharp/FsCheck/blob/master/Docs/Documentation.md
  // https://github.com/fsharp/FsUnit
  // https://code.google.com/p/unquote/
  open Xunit
  open FsUnit.Xunit
  open FsCheck
  
  // !all tests are failing
  // Note on FsCheck tests: The NUnit test runner will still green-light failing tests with Check.Quick 
  // even though it reports them as failing. Use Check.QuickThrowOnFailure instead.
  [<Fact>]
  let ``FsCheck test 1``() = Check.QuickThrowOnFailure(true <> false |@ sprintf "true = false")
  
  // types for FsCheck test 3 (registering an arbitrary type for generation)
  type EvenInt = 
    | EvenInt of int
    static member op_Explicit (EvenInt i) = i
  
  type ArbitraryModifiers = 
    static member EvenInt() = 
      Arb.from<int>
      |> Arb.filter (fun i -> i % 2 = 0)
      |> Arb.convert EvenInt int
  
  [<Fact>]
  let ``FsCheck test 3 (registering an arbitrary type for generation)``() = 
    Arb.register<ArbitraryModifiers>() |> ignore
    let ``generated even ints should be even`` (EvenInt i) = i % 2 = 0
    Check.QuickThrowOnFailure ``generated even ints should be even``
  
  [<Fact>]
  let ``FsCheck test 4 (and properties)``() = 
    Check.QuickThrowOnFailure((1 = 1) |@ sprintf "1 = 1" .&. (2 <> 3) |@ sprintf "2 != 3")
  
  [<Fact>]
  let ``FsCheck test 5 (or properties)``() = 
    Check.QuickThrowOnFailure((1 <> 2) |@ sprintf "1 != 2" .|. (2 <> 3) |@ sprintf "2 != 3")
  
  // type and spec for FsCheck test 6 (stateful testing)
  type Counter() = 
    let mutable n = 0
    member x.Inc() = n <- n + 1
    member x.Dec() = 
      (*if n > 2 then n <- n - 2
      else*)
      n <- n - 1
    member x.Get = n
    member x.Reset() = n <- 0
    override x.ToString() = n.ToString()
  
  //open FsCheck.Commands
  let spec = 
    let inc = 
      { new Command<Counter, int>() with
          
          member x.RunActual actual = 
            actual.Inc()
            actual
          
          member x.RunModel model = model + 1
          member x.Post(actual, model) = model = actual.Get |@ sprintf "model = %i, actual = %i" model actual.Get
          member x.ToString() = "inc" }
    
    let dec = 
      { new Command<Counter, int>() with
          
          member x.RunActual actual = 
            actual.Dec()
            actual
          
          member x.RunModel model = model - 1
          member x.Post(actual, model) = model = actual.Get |@ sprintf "model = %i, actual = %i" model actual.Get
          member x.ToString() = "dec" }
    
    { new ICommandGenerator<Counter, int> with
        member x.InitialActual = new Counter()
        member x.InitialModel = 0
        member x.Next _ = Gen.elements [ inc; dec ] }
  
  [<Fact>]
  let ``FsCheck test 6a (stateful testing spec)``() = Check.QuickThrowOnFailure(spec.ToProperty())
  
  // type and spec for FsCheck test 6b (stateful testing command series)
  let inc2 = 
    { new Command<Counter, int>() with
        
        member x.RunActual actual = 
          actual.Inc()
          actual
        
        member x.RunModel model = model + 1
        member x.Post(actual, model) = model = actual.Get |@ sprintf "model = %i, actual = %i" model actual.Get
        member x.ToString() = "inc" }
  
  let dec2 = 
    { new Command<Counter, int>() with
        
        member x.RunActual actual = 
          actual.Dec()
          actual
        
        member x.RunModel model = model - 1
        member x.Post(actual, model) = model = actual.Get |@ sprintf "model = %i, actual = %i" model actual.Get
        member x.ToString() = "dec" }
  
  let reset = 
    { new Command<Counter, int>() with
        
        member x.RunActual actual = 
          actual.Reset()
          actual
        
        member x.RunModel model = 0
        member x.Post(actual, model) = model = actual.Get |@ sprintf "model = %i, actual = %i" model actual.Get
        member x.ToString() = "reset" }
  
  let spec2 genList = 
    { new ICommandGenerator<Counter, int> with
        member x.InitialActual = new Counter()
        member x.InitialModel = 0
        member x.Next _ = Gen.elements genList }
  
  [<Fact>]
  let ``FsCheck test 6b (stateful testing command series)``() = 
    let ``inc, dec, reset`` = [ inc2; dec2; reset ]
    Check.QuickThrowOnFailure((spec2 ``inc, dec, reset``).ToProperty())
  
  [<Fact>]
  let ``FsUnit test 1``() = [ 1; 2; 3; 4 ] |> should equal [ 1; 2; 3; 4 ]
  
  [<Fact>]
  let ``FsUnit test 2``() = 2 |> should not' (equal 1)
  
  [<Fact>]
  let ``FsUnit test 3``() = 10.1 |> should (equalWithin 0.2) 10.22
  
  [<Fact>]
  let ``FsUnit test 4 (should throw exception)``() = 
    (fun () -> failwith "failed") |> should throw typeof<System.Exception>
//[<Fact>]
//let ``Unquote test 1``() =
//    test <@ ([3; 2; 1; 0] |> List.map ((+) 1)) = [1 + 3..1 + 0] @>
