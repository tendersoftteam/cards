﻿namespace Tendersoft.Bridge.Solver.Tests

module CoreTests = 
  open Xunit
  open FsUnit.Xunit
  open FsCheck
  open Tendersoft.Bridge.Engine
  open Core
  
  [<Fact>]
  let ``Bridge.Engine.Core test 0``() = 
    generateDeck
    |> List.length
    |> should equal 52
  
  [<Fact>]
  let ``Bridge.Engine.Core test 1``() = 
    let genDeck = generateDeck //|> List.toSeq
    let shuffDeck = generateSuffeldDeck() //|> List.toSeq
    Assert.NotEqual<Card>(genDeck, shuffDeck)
