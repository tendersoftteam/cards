﻿namespace Tendersoft.Bridge.Engine

module Core =
  open Tools
  
  //  type Suit = Spade | Heart | Diamond | Club
  [<StructuredFormatDisplay("{SuitString}")>]
  type Suit = 
    | S //♠ ♥ ♦ ♣
    | H
    | D
    | C
    override x.ToString() = sprintf "%s" x.SuitString
    member x.SuitString = 
      match x with
      | S -> "♠"
      | H -> "♥"
      | D -> "♦"
      | C -> "♣"
  
  //static member AllCases = allUnionCases<Suit>()
  let SuitString(suit : Suit) = suit.SuitString
  
  //  type Rank =
  //    | Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten
  //    | Jack | Queen | King | Ace
  [<StructuredFormatDisplay("{RankString}")>]
  type Rank = 
    | RA
    | RK
    | RQ
    | RJ
    | RT
    | R9
    | R8
    | R7
    | R6
    | R5
    | R4
    | R3
    | R2
    override x.ToString() = sprintf "%s" x.RankString
    member x.RankString = (toString x).[1].ToString()
  
  let RankString(rank : Rank) = rank.RankString
  
  let ranksComparer r0 r1 = 
    match r0, r1 with
    | a, b when a < b -> 1
    | a, b when a > b -> -1
    | a, b when a = b -> 0
    | _ -> failwith <| sprintf "rank comparisson encountered unknown input: a=%O b=%O)" r0 r1
  
  //static member AllCases = allUnionCases<Rank>()
  open Microsoft.FSharp.Reflection
  
  type Card = Rank * Suit
  
  let CardString(card : Card) = 
    let (r, s) = card
    RankString r + SuitString s
  
  type Hand = Card list
  
  type Deck = Card list
  
  type Side = //CardinalDirection
    | N
    | E
    | S
    | W
  
  type Side with
    static member EnumerateAll = allUnionCases<Side>()

  type PlayerPair =
    |NS
    |EW
  
  //  type PlayerPair = 
  //    NS of N// or S 
  //    | ES of E// or S
  let suitArray = allUnionCases<Suit>()
  let rankArray = allUnionCases<Rank>()
  
  // given suit returns index in array, 0-highest
  type SuitOrdering = Suit -> int
  
  let suitOrder suitOrderList suit = suitOrderList |> List.findIndex (fun s -> s = suit)
  
  let naturalSuitOrder : SuitOrdering = 
    suitArray
    |> Array.toList
    |> suitOrder
  
  let suitsComparerGeneral (suitOrdering : SuitOrdering) suit1 suit2 : int = 
    match (suitOrdering suit1), (suitOrdering suit2) with
    | a, b when a > b -> -1
    | a, b when a < b -> 1
    | a, b when a = b -> 0
    | _ -> failwith "Suit comparisson failed"
  
  let suitsComparerNatural = suitsComparerGeneral naturalSuitOrder
  
  let suitOrderingWithAtu atu vistCard = 
    match atu, vistCard with
    | None, (_, cs) -> 
      cs :: (suitArray
             |> Array.toList
             |> List.except [ cs ])
    | Some s, (_, cs) -> 
      s :: cs :: (suitArray
                  |> Array.toList
                  |> List.except [ cs; s ])
  
  let suitsComparerWithAtu atu vistCard = 
    suitOrderingWithAtu atu vistCard
    |> suitOrder
    |> suitsComparerGeneral
  
  type Atu = Suit option
  
  type Contract = int * Atu
  
  type Auction = 
    | Pass
    | Bid of Contract
    | Double
    | Redouble
  
  let generateDeck : Deck = 
    let ranks = rankArray |> Array.toList
    let suits = suitArray |> Array.toList
    //crossproduct suits ranks
    cartesian ranks suits
  
  let newDeck = 
    //    [(Two, Spade); (Three, Spade); (Four, Spade); (Five, Spade); 
    //    (Six, Spade); (Seven, Spade); (Eight, Spade); (Nine, Spade); 
    //    (Ten, Spade); (Jack, Spade); (Queen, Spade); (King, Spade); (Ace, Spade);
    //    (Two, Heart); (Three, Heart); (Four, Heart); (Five, Heart); 
    //    (Six, Heart); (Seven, Heart); (Eight, Heart); (Nine, Heart); 
    //    (Ten, Heart); (Jack, Heart); (Queen, Heart); (King, Heart); (Ace, Heart);
    //    (Two, Club); (Three, Club); (Four, Club); (Five, Club); 
    //    (Six, Club); (Seven, Club); (Eight, Club); (Nine, Club); 
    //    (Ten, Club); (Jack, Club); (Queen, Club); (King, Club); (Ace, Club);
    //    (Two, Diamond); (Three, Diamond); (Four, Diamond); (Five, Diamond); 
    //    (Six, Diamond); (Seven, Diamond); (Eight, Diamond); (Nine, Diamond); 
    //    (Ten, Diamond); (Jack, Diamond); (Queen, Diamond); (King, Diamond); (Ace, Diamond)]
    generateDeck
  
  let shuffle deck = 
    let random = new System.Random()
    deck |> List.sortBy (fun _ -> random.Next())
  
  let isUnique deck = 
    let baseDeckSet = newDeck |> Set.ofList
    try 
      let deckSet = deck |> Set.ofList
      deckSet.IsSubsetOf baseDeckSet && deckSet.IsSupersetOf baseDeckSet
    with _ -> false
  
  let generateSuffledDeck() = generateDeck |> shuffle
  let deckStrings deck = deck |> List.map CardString
  
  type Trick = Card (*ref*) list // 4 Cards //refs //{S:Hand;W:Hand;N:Hand;E:Hand}
  
  type Tricks = Trick list // 13 Tricks
  
  type TrickWin = bool
  
  type Player = 
    { Name : string
      Side : Side
      Hand : Hand
      (*ref*)
      //Tricks : (Card * TrickWin) list // optimization saving state
      }
  
  let createPlayer side = 
    { Name = toString side
      Side = side
      Hand = []
      (*Tricks = []*) }
  
  type Player with
    static member create = createPlayer
  
  let findPlayerIndex players side =
    players |> Seq.findIndex (fun p->p.Side=side)

  let giveHand p h =
    {p with Hand=h}

  let giveHands (players:Player array) (sh:(Side*Hand) list)=
    for i=0 to players.Length-1 do
      let pi = findPlayerIndex players (fst sh.[i])
      players.[pi] <- giveHand players.[pi] (snd sh.[i])

  type Deal = Deck -> (Card * Deck)
  
  type PickupCard = Hand * Card -> Hand
  
  type Vulnerable = 
    | NS
    | EW
    | Both
  
  type Table = 
    { Players : Player array
      Contract : Contract
      Tricks : Tricks
      Dealer : Player
      Vulnerable : Vulnerable option
      Auction : Auction list }
  type GameResult = {TricksDelta:int}
  
  type AuctionPreSearch = 
    | Pass3
    | PassX of int * Auction
  
  type AuctionSearch = 
    | LastBid of Atu
  
  let findFinishFolder s t : AuctionPreSearch option = 
    match s, t with
    | Some(PassX(a, Pass)), Pass when a = 3 -> None
    | Some(PassX(a, Pass)), _ when a = 3 -> Some Pass3
    | Some(PassX(a, Pass)), Pass when a < 3 -> Some(PassX(a + 1, Pass))
    | _ -> failwith (sprintf "unknown (%A,%A) while searching for declarer" s t)
  
  let safePlayerIndex limit i = i % limit
  
  let findFirstAtu auctionList a0 = 
    auctionList |> List.findIndex (fun a -> 
                     match a0, a with
                     | Bid(_, c), Bid(_, b) -> b = c
                     | _ -> false)
  
  let findDeclarer table = 
    if table.Auction.IsEmpty then Some table.Players.[0]
    else 
      let auctionFinishResult = 
        table.Auction
        |> Seq.rev
        |> Seq.take 4
        |> Seq.fold findFinishFolder (Some <| PassX(0, Pass))
      
      let auctionResult = 
        match auctionFinishResult with
        | None -> None
        | Some Pass3 -> 
          table.Auction
          |> Seq.rev
          |> Seq.skip 3
          |> Seq.find (fun a -> 
               match a with
               | Bid _ -> true
               | _ -> false)
          |> findFirstAtu table.Auction
          |> fun i -> (table.Players |> Array.findIndex (fun p -> p = table.Dealer)) + i
          |> safePlayerIndex table.Players.Length
          |> fun si -> table.Players.[si]
          |> Some
        | _ -> failwith (sprintf "Auction result (%A) is invalid" auctionFinishResult)
      
      auctionResult
  
  type Table with
    member x.FindDeclarer = findDeclarer x
  
  let findDeclarerIndex table = 
    match findDeclarer table with
    | None -> failwith "No declarer available"
    | Some p0 -> table.Players |> Array.findIndex (fun p -> p = p0)
  
  type Table with
    member x.FindDeclarerIndex  = findDeclarerIndex x
  
  (*ref*)
  //
  type Game = 
    { Deck : Deck
      Table : Table }
  
  let dealFromDeck : Deal = 
    function 
    | [] -> failwith "No more cards in deck to deal"
    | x :: deck -> x, deck
  
  //WARN:mutates state
  let dealDeck deck table = 
    let safeIndex = safePlayerIndex table.Players.Length
    
    let rec deal deck i = 
      let safeI = safeIndex i
      let card, deck = dealFromDeck deck
      table.Players.[safeI] <- { table.Players.[safeI] with Hand = card :: table.Players.[safeI].Hand }
      deal deck i + 1
    deal deck 0
  
  let dealDeck2 deck table = 
    let indexedDeck = deck |> List.indexed
    
    let dealToPlayer i = 
      let hand = 
        indexedDeck
        |> List.filter (fun (j, _) -> j % 4 = i)
        |> List.map (fun (_, x) -> x)
      { table.Players.[i] with Hand = hand }
    { table with Players = Array.init (table.Players.Length) (fun i -> dealToPlayer i) }
  
  let dealDeck3 game = { game with Table = dealDeck2 game.Deck game.Table }
  
  // card ordering function
  let cardsComparerGeneral (suitOrdering : SuitOrdering) (card1 : Card) (card2 : Card) : int = 
    match card1, card2 with
    | (r0, s0), (r1, s1) when s0 = s1 -> ranksComparer r0 r1
    | (_, s0), (_, s1) -> suitsComparerGeneral suitOrdering s0 s1
  
  //      match suitOrdering s0, suitOrdering s1 with
  //      | (so0, so1) when so0 > so1 -> 1
  //      | (so0, so1) when so0 < so1 -> -1
  //      | _ -> 0
  let cardsComparerNatural (c1 : Card) (c2 : Card) : int = cardsComparerGeneral naturalSuitOrder c1 c2
  
  let cardsComparerWithAtu atu vistCard = 
    suitOrderingWithAtu atu vistCard
    |> suitOrder
    |> cardsComparerGeneral
  
  let sortTrickCards atu (cards : Card list) : Card list = 
    //    let tricksComparer atu vistCard card0 card1:int=
    //      match atu,vistCard,card0,card1 with
    //      |None,(_,s),(_,s0),(_,s1) when s0<>s1 && s=s0 ->1
    //      |None,(_,s),(_,s0),(_,s1) when s0<>s1 && s=s1 ->-1
    //      |None,(r,s),(r0,s0),(r1,s1) when s0==s1 && s=s0 ->match r,r0,r1 with
    //      |Some a,(_,s),(_,s0),(_,s1) when s0=s1 && s<>s1 && s1<>a ->0
    //      |None,s when s=c
    let suitOrdering : SuitOrdering = 
      fun suit -> 
        match atu, cards with
        | None, (_, vs) :: _ when vs = suit -> 0
        | None, _ -> 1
        | Some s, _ when s = suit -> 0
        | Some _, (_, vs) :: _ when vs = suit -> 1
        | Some _, _ -> 2
    
    //| _ -> failwith "suit ordering not possible"
    //    let suitOrderList = suitOrderingWithAtu atu (List.head cards) // suitOrdering atu
    //    let suitOrderer = suitOrder suitOrderList
    let suitOrderer = suitOrdering
    let cardsComparer = cardsComparerGeneral suitOrderer
    
    //let cardsComparer = cardsComparerWithAtu atu (List.head cards)
    let cardOrdered = 
      cards
      |> List.sortWith cardsComparer
      |> List.rev
    cardOrdered

  let sortHandCards atu (cards : Card list) : Card list = 
    let suitOrdering : SuitOrdering = 
      let findCardIndex suit cards0=
        cards0 |> List.countBy (fun (_,s)->s) |> List.sortByDescending (fun (_,c)->c) |> List.findIndex (fun (s,_)->s=suit)
      fun suit -> 
        match atu, cards with
        | None, xs -> xs |> findCardIndex suit
        | Some s, _ when s = suit -> 0
        | Some s0, xs -> xs |> List.filter (fun (_,s)->s<>s0) |> findCardIndex suit |> (+) 1

    let cardsComparer = cardsComparerGeneral suitOrdering
    cards
      |> List.sortWith cardsComparer
      |> List.rev
  
  let sortCards cards : Card list = 
    cards
    |> List.sortWith cardsComparerNatural
    |> List.rev
  
  let orderPlayersGeneral (po : Side array) players : Player array = 
    if Array.length po <> Array.length players then 
      failwith "ordering array has different number of elements than ordered array"
    Array.init (Array.length players) (fun i -> players |> Array.find (fun p -> p.Side = po.[i]))
  
  let orderPlayersNatural = orderPlayersGeneral [| Side.S; Side.W; Side.N; Side.E |]
  
  let createTable players contract = 
    let orderedPlayers = orderPlayersNatural players
    { Players = orderedPlayers
      Contract = contract
      Tricks = []
      Dealer = orderedPlayers.[0]
      Vulnerable = Option<Vulnerable>.None
      Auction = [] }
  
  type Table with
    static member create = createTable
  
  let sortTableHands table = 
    let initFunc = fun i -> { table.Players.[i] with Hand = sortCards table.Players.[i].Hand }
    { table with Players = Array.Parallel.init table.Players.Length initFunc }
  
  //if no cards, play whatever
  //if any card, play suit - if have no suit, play whatever
  //whatever: atu, any suit
  let vistVariants (atu : Atu) (hand : Hand) (trick : Trick) : Card seq = 
    let tableCards = trick //|> List.map (!)
    
    let vists = 
      match tableCards with
      | [] -> 
        hand
        |> sortHandCards atu
        |> List.toSeq
      | (_, s0) :: _ -> 
        let matchingSuits = 
          hand
          |> List.filter (fun (_, s) -> s = s0)
          |> sortCards
        match matchingSuits with
        | [] -> 
          hand
          |> sortHandCards atu
          |> List.toSeq
        | _ -> matchingSuits |> List.toSeq
    vists
  
  let pickupCard : PickupCard = 
    function 
    //| [], _ -> []
    | hand, c when (hand |> List.contains c) -> hand |> List.except [ c ] //.where (fun c0 -> c <> c0)
    | _ -> failwith "Card not on hand"
  
  let getTricks (trick) (vistPossibilities : seq<Card>) : seq<Trick> = 
    seq { 
      for vp in vistPossibilities do
        yield trick @ [ (*ref*) vp ]
    }
  
  let getTricksAndHands trick hand vists : seq<Trick * Hand> = 
    vists |> Seq.map (fun c -> trick @ [ (*ref*) c ], pickupCard (hand, c))

  let playPossibleVists atu tableCards hand=  
    let vists = vistVariants atu hand tableCards
    let tricksAndHands = getTricksAndHands tableCards hand vists
    tricksAndHands
  
  let winningPlayerIndex players atu vistingPlayerIndex currentTrick = 
    let cs = sortTrickCards atu currentTrick
    let bestCard = List.head cs
    let trickWinIndex = currentTrick |> List.findIndex (fun x -> x = bestCard)
    let safePIndex = safePlayerIndex (Array.length players)
    safePIndex (trickWinIndex + vistingPlayerIndex)
  
  let isTablePlayFinished table : bool = table.Players |> Array.forall (fun p -> p.Hand.Length = 0)
  
  let nextPlayerIndex table : int option = 
    if isTablePlayFinished table then None
    else 
      let _, atu = table.Contract
      let wpiWithPlayersAndAtu = winningPlayerIndex table.Players atu
      
      let lastWpi = 
        table.Tricks
        |> List.rev
        |> List.fold wpiWithPlayersAndAtu table.FindDeclarerIndex
      Some lastWpi
  
  type Table with
    member x.NextPlayerIndex = nextPlayerIndex x
  
  //(trickWinIndex + vistingPlayerIndex) % (Array.length players) //4 for bridge game
  // returning also the last winning player's index might be usefull and is possible
  let rec playTrickRound table vistingPlayerIndex : Table = 
    let _, atu = table.Contract
    let safePIndex = safePlayerIndex table.Players.Length
    
    let rec playPlayerTrick vpi i tableCards = 
      let safeVpi = safePIndex vpi
      let currPlayer = table.Players.[safeVpi]
      let vists = vistVariants atu currPlayer.Hand tableCards
      //let vist1 = Seq.head vists
      let trick, hand = getTricksAndHands tableCards currPlayer.Hand vists |> Seq.head
      table.Players.[safeVpi] <- { currPlayer with Hand = hand }
      if (i + 1) < table.Players.Length then playPlayerTrick (vpi + 1) (i + 1) trick
      else trick
    
    let fullTrick = playPlayerTrick vistingPlayerIndex 0 []
    let table = { table with Tricks = fullTrick :: table.Tricks }
    //      match table.Tricks with
    //      | [] -> []
    //      | x :: _ -> x
    let wpi = winningPlayerIndex table.Players atu vistingPlayerIndex fullTrick
    //TODO: update players' tricks list
    let safeWpi = safePIndex wpi
    if List.isEmpty table.Players.[safeWpi].Hand then table
    else playTrickRound table wpi
  
  //  let playTrick table : Table =
  //    //let npi= table.NextPlayerIndex
  //
  //    Seq.empty |> Seq.head

  let pileupTricks tricks trick=
    match tricks with
      | [] -> [ trick ]
      | x :: xs when List.length x = 4 -> trick :: x :: xs
      | x :: xs when List.length x < 4 -> trick :: xs
      | _ -> failwith (sprintf "cannot update table tricks: %A" tricks)

  let updateTableTrickAndCurrHand table currPlayerIndex trick hand = 
    let updPlayer = { table.Players.[currPlayerIndex] with Hand = hand }
    
    let players = 
      table.Players |> Array.mapi (fun i p -> 
                         if i = currPlayerIndex then updPlayer
                         else p)
    
    let tricks = pileupTricks table.Tricks trick
    
    { table with Tricks = tricks
                 Players = players }

  let currentTrick tricks =
    match tricks with
    | [] -> []
    | x :: _ when List.length x < 4 -> x
    | x :: _ when List.length x = 4 -> []
    | _ -> failwith (sprintf "cannot infer table cards from table tricks: %A" tricks)
  type Table with
    member x.CurrentTrick = currentTrick x.Tricks

  let lastTrick tricks =
    match tricks with
    | [] -> []
    | x :: _ -> x
  type Table with
    member x.LastTrick = lastTrick x.Tricks

  let isDeclarerHandEmpty table=
    table.Players.[table.FindDeclarerIndex].Hand |> List.isEmpty

//  let updatePlayersWinTricks vpi t =
//    let _,atu = t.Contract
//    let lt = t.LastTrick
//    let wpi = winningPlayerIndex t.Players atu vpi lt
//    let ps = t.Players |> Array.mapi (fun i p->{p with Tricks=(lt.[i],i=wpi)::p.Tricks })
//    {t with Players = ps}

  let generatePlayedTrickTables table : Table seq = 
    //each player generates tables with possible vists
    let _, atu = table.Contract
    let safePIndex = safePlayerIndex table.Players.Length
    
    let rec playPlayerTrick vpi i currTable = 
      let tableCards = currentTrick currTable.Tricks      
      let safeVpi = safePIndex vpi
      let currPlayer = currTable.Players.[safeVpi]
      let tricksAndHands =playPossibleVists atu tableCards currPlayer.Hand
      let playedTables = 
        tricksAndHands |> Seq.map (fun (trick, hand) -> updateTableTrickAndCurrHand currTable safeVpi trick hand)
      if (i + 1) < currTable.Players.Length then 
        playedTables
        |> Seq.map (fun t -> playPlayerTrick (vpi + 1) (i + 1) t)
        |> Seq.concat
      else playedTables //|> Seq.map (updatePlayersWinTricks safeVpi)

    match table.NextPlayerIndex with
    | None -> Seq.singleton table
    | Some npi -> playPlayerTrick npi 0 table
  
  let attackersI table =
    let safePI = safePlayerIndex table.Players.Length
    let declarerIndex = findDeclarerIndex table
    declarerIndex, safePI (declarerIndex+2)

  let defendersI table =
    let safePI = safePlayerIndex table.Players.Length
    let x,y = attackersI table
    safePI (x+1), safePI (y+1)

  let attackers table =
    match findDeclarer table with
    |None -> None
    |Some p -> 
      match p.Side with
      | Side.E|Side.W->Some PlayerPair.EW
      |Side.N|Side.S->Some PlayerPair.NS

  let defenders table =
    match attackers table with
    |None->None
    |Some side -> 
      match side with
      |PlayerPair.EW->Some PlayerPair.NS
      |PlayerPair.NS->Some PlayerPair.EW

  let playerTrickWinCount table i =
    let _, atu = table.Contract
    let wpiWithPlayersAndAtu = winningPlayerIndex table.Players atu
      
    let playerTrickCount=
      table.Tricks
      |> List.rev
      |> List.scan wpiWithPlayersAndAtu table.FindDeclarerIndex
      |> List.skip 1//the seed state
      |> List.filter ((=)i)
      |> List.length
    playerTrickCount

//  let playerTrickWinCount2 table i =
//    if table.Players.[i].Tricks |> List.isEmpty then playerTrickWinCount table i
//    else table.Players.[i].Tricks |> List.filter (fun (_,w)->w) |> List.length

  let playResult table =
    let x,y = attackersI table
    let attackersTrickCount = playerTrickWinCount table x + playerTrickWinCount table y
    let i,_=table.Contract
    {TricksDelta = attackersTrickCount-i-6}
  type Table with member x.PlayResult=playResult x

  let playResultDiff table=
    let x,y = attackersI table
    let attackersTrickCount = playerTrickWinCount table x + playerTrickWinCount table y
    attackersTrickCount- table.Tricks.Length

  let pairTrickSeq pp table =
    let x,y=
      match pp with    // hardcoded defenders/attackers sides
      |PlayerPair.EW->defendersI table
      |PlayerPair.NS->attackersI table
    table.Tricks
    |> List.map (fun tr->tr.[x],tr.[y])

  //enumerate all possible tables
  #if DEBUG2
  let rec possibleTables i table =
  #else
  let rec possibleTables table =
  #endif 
    let tablesAfterNextRound = generatePlayedTrickTables table // Seq.empty<Table> //invoke tables with possible plays for vists
    if table.Players.[0].Hand |> List.isEmpty then 
      #if DEBUG2
      printf "Table %d generated" i
      #endif
      tablesAfterNextRound
    else 
      tablesAfterNextRound      
      #if DEBUG2
      |> Seq.map (possibleTables (i+1))
      #else
      |> Seq.map possibleTables
      #endif 
      |> Seq.concat
  
  let sortTablePlayers table = { table with Players = orderPlayersNatural table.Players }
  
  open ListEx
  
  let solve (table : Table) : Table seq = 
    // fix ordering
    let table0 = sortTablePlayers table
    // sort cards on hand (optional)
    let table0 = sortTableHands table0
    #if DEBUG2
    let tables = possibleTables 0 table0
    #else
    let tables = possibleTables table0 //|> Seq.filter (fun t->t.Tricks.Length=13)
    #endif
    tables
  
  let solve2 (table : Table) : Tricks seq = solve table |> Seq.map (fun t -> t.Tricks)
  
  open System.Collections.Generic
  let rec possibleTables2 (filterCriteria:Table->bool) (uniqueSeqs:HashSet<(Card*Card) list>) table =
    let tablesAfterNextRound = 
      generatePlayedTrickTables table //invoke tables with possible plays for vists    
      |> Seq.filter filterCriteria
      |> Seq.filter (pairTrickSeq PlayerPair.NS >> uniqueSeqs.Add >> not)
    
    if table |> isDeclarerHandEmpty then
      tablesAfterNextRound
    else 
      tablesAfterNextRound  
      |> Seq.map (possibleTables2 filterCriteria uniqueSeqs)
      |> Seq.concat

  let solve3 table :Table seq=  
    sortTablePlayers table
    |> sortTableHands
    |> possibleTables2 (fun t->playResultDiff t >= -1) (new HashSet<(Card*Card) list>())

  let randomlyPlayTable table:Table=
    // each time vist a random single valid card
    // note for GP: encode table state as Chromosome
    //              each vist should be based on precious Seq<Table> states
    table

  //convert Wspolny Jezyk to decision tree (WJDT)
  //generate random hands
  //ask for licitation input
  //evaluate using WJDT
  //break on error, show correct
  //TODO:
  //implement cards' value counting (per hand)
  let cardStrenght = function
    | (r, _) when r = Rank.RA -> 4
    | (r, _) when r = Rank.RK -> 3
    | (r, _) when r = Rank.RQ -> 2
    | (r, _) when r = Rank.RT -> 1
    | _ -> 0

  let countHandStrength hand = 
    hand |> List.fold (fun strength card -> strength + cardStrenght card) 0
