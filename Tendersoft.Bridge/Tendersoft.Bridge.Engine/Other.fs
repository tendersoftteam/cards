﻿module Other

module CardGameWarDomain =
   type Suit = Club | Diamond | Spade | Heart
   type Rank =
     | Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten
     | Jack | Queen | King | Ace
   type Card = Suit * Rank
   type Deck = Card list
   type PlayerName = string
   type Player = {Name:PlayerName; Deck:Deck}
   type Pile = (PlayerName option * Card list) list
   type Deal = (Deck * Player list) -> Player list
   type Battle = Player list -> (Player list * Pile)
   type War = (Player list * Pile) -> (Player list * Pile)
   type PickPile = (Player * Pile) -> Player
   type Game = (Deck * Player list) -> Player

module Utils =
  open Microsoft.FSharp.Reflection
  
  let rand = System.Random()
  
  let unionCases<'a>() =
    FSharpType.GetUnionCases(typeof<'a>)
    |> Array.map (fun x -> FSharpValue.MakeUnion(x, [||]) :?> 'a)
    |> Array.toList
    
  let cartProd xs ys =
    xs |> List.collect (fun x -> ys |> List.map (fun y -> x,y))
    
  let swap (a: _[]) x y =
    let t = a.[x]
    a.[x] <- a.[y]
    a.[y] <- t
    
  let shuffle xs = // Knuth's shuffle algorithm
    let xs' = xs |> List.toArray
    xs' |> Array.iteri(fun i _ -> swap xs' i (rand.Next(i, xs'.Length)))
    xs' |> Array.toList

module CardGameWar =
  
  open System
  open CardGameWarDomain
  open Utils
  
  let rec round players cards (pile:Pile) = function
    | [] ->
      let tags = players |> List.map(fun x -> x.Name |> Some)
      let cards' = cards |> List.map(fun x -> [x])
      let _,cards'' = pile |> List.unzip
      let tagAndCards = (None, cards'' |> List.fold(fun a x -> a @ x) [])
      players,tagAndCards::((tags,cards') ||> List.zip)
    | p::players' ->
      match p.Deck with
        | [] -> round players cards pile players'
        | c::cards' ->
          let p' = {p with Deck = cards'}
          round (p'::players) (c::cards) pile players'
          
  let battle : Battle =
    fun (players : Player list) ->
      players |> round [] [] []
      
  let rec war : War = // call round twice to skip first card
    fun (players : Player list, pile : Pile) ->
      let players', pile' = players |> round [] [] pile 
      (pile',players') ||> round [] []
      
  let pickPile : PickPile =
    fun (player: Player, pile: Pile) ->
      printfn "player: %A, pile: %A" (player.Name) pile
      let _,cs = pile |> List.unzip
      {player with Deck = player.Deck @ (cs |> List.reduce(@))}
      
  let deck () : Deck =
    (unionCases<Suit>(), unionCases<Rank>()) ||> cartProd |> shuffle
    
  let players n : Player list =
    let ns = Array.append [|'a' .. 'z'|] [|'A' .. 'Z'|]
    
    let rec players' acc = function
      | 0 -> acc
      | i when i <= 4 ->
        let p = {Name = string ns.[i-1]; Deck = []}
        players' (p::acc) (i-1)
      | _ -> failwith "Only max 4 players are allowed"
    players' [] n
    
  let deal : Deal =
    fun (deck : Deck, players : Player list) ->
      let n = players |> List.length
      let rec deal' (ps : Player list) = function
        | [] -> ps
        | c::cs ->
          let p,ps' = ps |> List.head, ps |> List.tail
          let p' = {p with Deck = c::p.Deck}
          deal' (p'::ps' |> List.permute(fun i -> (i + 1) % n)) cs
      deck |> deal' players
      
  let game : Game =
    fun (deck : Deck, players : Player list) ->
      let ps = deal(deck, players)
      
      let rec game' (pile : Pile) = function
        | []  -> failwith "No winners" 
        | [p] -> p
        | ps' ->
          let ps'',pile' =
            match pile |> List.isEmpty with
              | true  ->  ps'       |> battle
              | false -> (ps',pile) |> war
          // based on win or loose -> check pile for 1 only high card
          // then add pile to winner or pass pile and plays to next "war".
          let max =
            pile'
            |> List.filter(fun (x,y) -> x.IsSome)
            |> List.maxBy(fun (x,y) -> y |> List.head |> snd)
            |> fun (x,ys) -> ys |> List.head |> snd
          let winner =
            pile'
            |> List.filter(fun (x,y) -> x.IsSome)
            |> List.filter(fun (x,ys) -> (ys |> List.head |> snd) = max)
          let ps''',pile'' =
            let n = ps'' |> List.length
            
            let rec findPlayer tag = function
              | x::xs when (x.Name = tag) -> x,xs
              | xs -> findPlayer tag (xs |> List.permute(fun i -> (i + 1) % n))
              
            match winner |> List.length = 1 with
              | true ->
                let tag,_ = winner |> List.head
                let tag' = tag |> function | Some v -> v | None -> String.Empty
                let h,t = ps'' |> findPlayer tag'
                ((h,pile') |> pickPile)::t,[]
              | false -> ps'',pile'
          
          game' pile'' ps'''
          
      ps |> game' []
      
  let simulation n = (deck (),players n) |> game

