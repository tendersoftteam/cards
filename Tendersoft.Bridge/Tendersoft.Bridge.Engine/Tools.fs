﻿namespace Tendersoft.Bridge.Engine

module Tools = 
  open Microsoft.FSharp.Reflection
  
  let toString (x : 'a) = 
    match FSharpValue.GetUnionFields(x, typeof<'a>) with
    | case, _ -> case.Name
  
  let fromString<'a> (s : string) = 
    match FSharpType.GetUnionCases typeof<'a> |> Array.filter (fun case -> case.Name = s) with
    | [| case |] -> Some(FSharpValue.MakeUnion(case, [||]) :?> 'a)
    | _ -> None
  
  // Usage:
  // type A = X|Y|Z with
  //     member this.toString = toString this
  //     static member fromString s = fromString<A> s
  // > X.toString;;
  // val it : string = "X"
  // > A.fromString "X";;
  // val it : A option = Some X
  // > A.fromString "W";;
  // val it : A option = None
  // > toString X;;
  // val it : string = "X"
  // > fromString<A> "X";;
  // val it : A option = Some X
  let allUnionCases<'T>() = 
    FSharpType.GetUnionCases(typeof<'T>) |> Array.map (fun case -> FSharpValue.MakeUnion(case, [||]) :?> 'T)
  
  let crossproduct l1 l2 = 
    seq { 
      for el1 in l1 do
        for el2 in l2 do
          yield el1, el2
    }
  
  let cartesian xs ys = xs |> List.collect (fun x -> ys |> List.map (fun y -> x, y))
  
  /// Colored printf
  let cprintf fc bc fmt = 
    let finalFunc (s : string) = 
      let old0 = System.Console.BackgroundColor
      let old = System.Console.ForegroundColor
      try 
        System.Console.ForegroundColor <- fc
        System.Console.BackgroundColor <- bc
        System.Console.Write s
      finally
        System.Console.ForegroundColor <- old
        System.Console.BackgroundColor <- old0
    Printf.kprintf finalFunc fmt
  
  // Colored printfn
  let cprintfn fc bc fmt = 
    cprintf fc bc fmt
    printfn ""
