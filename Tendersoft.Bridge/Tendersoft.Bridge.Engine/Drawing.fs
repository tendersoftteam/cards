﻿  namespace Tendersoft.Bridge.Engine
  
  module Drawing = 
    open Tools
    open Core
    open System
    
    //open System.Drawing
    let suitColorList = 
      [ Suit.S, ConsoleColor.Black
        Suit.H, ConsoleColor.Red
        Suit.D, ConsoleColor.DarkYellow
        Suit.C, ConsoleColor.Green ] //Color.Orange;Suit.C,Color.LightGreen]
    
    let suitColorDict = 
      suitColorList
      |> List.map (fun (s, fc) -> s, cprintf fc ConsoleColor.White)
      |> dict
    
    let printCard (card : Card) = 
      let printer (_, s) = suitColorDict.Item s
      printer card "%A" (CardString card)
      printf " "
    
    let printDeck (deck : Deck) = 
      let cardsWithColors = deck |> List.map (fun (r, s) -> (CardString(r, s)), suitColorDict.Item s)
      for (cardString, printer) in cardsWithColors do
        printer "%s" cardString
        printf " "
      printfn ""
    
    let printDeck2 (deck : Deck) = 
      for card in deck do
        printCard card
      printfn ""
